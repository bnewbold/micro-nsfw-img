
import os
import json
import tempfile

from nsfw_detector import predict
from flask import Flask, flash, request, redirect, url_for, abort, jsonify
from werkzeug.utils import secure_filename
import tensorflow
from tensorflow import keras
from PIL import Image

IMAGE_DIM = (224, 224)

app = Flask(__name__)

nsfw_model = predict.load_model('./nsfw_mobilenet2.224x224.h5')

@app.route('/classify-image', methods=['POST'])
def classify_image():
    if 'file' not in request.files:
        abort(400, "missing file in POST")
    raw = request.files['file']

    # HACK: write file to disk and process from there, to work around wrangling
    # pixel arrays into the correct tensor shape
    with tempfile.NamedTemporaryFile() as tmp:
        tmp.write(raw.read())
        res = predict.classify(nsfw_model, tmp.name)

    # remove the outer file key and return a simple object
    resp = dict()
    for k in res.keys():
        resp = res[k]

    print(f"{raw.filename}: " + json.dumps(resp, sort_keys=True))

    return jsonify(resp)
