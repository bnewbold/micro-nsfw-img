
FROM tensorflow/tensorflow:2.11.0

WORKDIR /
RUN pip install Flask nsfw_detector

COPY /nsfw_mobilenet2.224x224.h5 /
COPY /micro_nsfs_img/ /micro_nsfs_img/

ENV PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python
EXPOSE 5000/tcp

CMD ["python3", "-m", "micro_nsfs_img"]

LABEL org.opencontainers.image.source=https://gitlab.com/bnewbold/micro-nsfw-img
LABEL org.opencontainers.image.description="Simple NSFW img classifier with outdated tech"
LABEL org.opencontainers.image.licenses=MIT
