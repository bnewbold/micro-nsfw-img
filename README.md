
Simple NSFW image classifier, using out-of-date tech. The intent of this is to
have a bare-bones, easy to run alternative to more robust SaaS and cloud
solutions for image detection.

This might be helpful for you in development and testing, but please don't rely
on this in the realy world. Unlikely to respond to security, quality, bias, or
other issues with this repository.

Builds on <https://github.com/GantMan/nsfw_model>.


## Quickstart with Docker

Fetch docker image (it is huge, almost 4 GBytes) and run:

    docker pull bnewbold/micro-nsfw-img:latest
    docker run --network host bnewbold/micro-nsfw-img

Post an image:

    http --multipart post :5000/classify-image file@example.png

    [...]
    {
        "drawings": 0.01377286110073328,
        "hentai": 0.0027025321032851934,
        "neutral": 0.9812405705451965,
        "porn": 0.0020549497567117214,
        "sexy": 0.00022897876624483615
    }


## Development

Download (old) model:

    wget https://s3.amazonaws.com/ir_public/nsfwjscdn/nsfw_mobilenet2.224x224.h5

Setup venv and deps:

    python3 -m venv .venv
    source .venv/bin/activate

    pip install Flask
    pip install nsfw_detector
    pip install tensorflow

Run the service like:

    export PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python
    python -m micro_nsfs_img

Build docker image:

    docker build . -t bnewbold/micro-nsfw-img:latest

Unfortunately the docker build in-efficiently re-installs the tensorflow python
package, resulting in a huge image size.

Publish docker image to registry:

    docker push bnewbold/micro-nsfw-img:latest
